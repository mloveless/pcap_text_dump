# pcap_text_dump - Dump all text from a PCAP file.

---

## Description

This is a simple program for taking a PCAP file as input and outputting the
plain text to standard out. Optionally either plaintext web traffic (HTTP) or
plaintext email traffic (SMTP) can be singled out. This is a command line
utility.

Easy compile on Linux:

`gcc -o pcap_text_dump pcap_text_dump.c`

## Notes

This is more of a coding exercise than a practical application. It was coded
in a day, although a few of the routines were cut and pasted from older code
(which more or less inspired the entire project).

Next steps might include a plaintext search, or a port to another language
(or two).


-Mark

ml@markloveless.net 
2020May25

