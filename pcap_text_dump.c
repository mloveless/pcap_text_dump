/* pcap_probe - compile using gcc -o pcap_probe pcap_probe.c */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define MAXBUF 2000
#define MODE_READ 0
#define MODE_WRITE 1

/* handy shortcuts */
typedef unsigned char BYTE;
typedef unsigned int uint32;
typedef unsigned short int uint16;

/* structs for dealing with pcap file structure */
typedef struct pcap_hdr_s { 
	uint32 magic_number;   /* magic number */
	uint16 version_major;  /* major version number */
	uint16 version_minor;  /* minor version number */
	uint32 thiszone;       /* GMT to local correction */
	uint32 sigfigs;        /* accuracy of timestamps */
	uint32 snaplen;        /* max length of captured packets, in octets */
	uint32 network;        /* data link type */
} pcap_hdr_t;

typedef struct pcaprec_hdr_s { 
	uint32 ts_sec;         /* timestamp seconds */
	uint32 ts_usec;        /* timestamp microseconds */
	uint32 incl_len;       /* number of octets of packet saved in file */
	uint32 orig_len;       /* actual length of packet */
} pcaprec_hdr_t;

/* this is the chained struct for individual streams in a single pcap file */
struct streams
{
	char *filename;
	uint32 offset;
	uint32 srcip;
	uint16 srcport;
	uint32 dstip;
	uint16 dstport;
	struct streams *next;
};

struct streams *streamnames;
struct streams *streamtail;

/* global variables */
int verbose;       // are we being verbose?
int e_flag;	   // are we looking for SMTP (email)?
int w_flag;        // are we looking for web traffic?
char *prot;        // protocol we're dealing with
BYTE targ_port[1]; // target port

/* some compilers optimize out memset, so we do this instead */
void *guaranteed_memset(void *v,int c,size_t n)
{
	volatile char *p=v;
	while (n--) *p++=c;
	return v;
}

/* simple routine to return file handle */
FILE* get_file_handle(char * file, int mode)
{
	FILE* f;
	if(mode == MODE_READ)
		f = fopen(file,"rb");
	else
		f = fopen(file,"wb");
	if (f == NULL)
	{
		fprintf(stderr,"ERROR: cannot open file for reading: %s\n",file);
		exit(-1);
	}
	return f;
}

/* Build the streams list struct chain */
void populate_streams(char * filename,uint32 offset,uint32 srcip,uint16 srcport,uint32 dstip,uint16 dstport)
{
	if (streamnames == NULL)
	{
		streamnames = (struct streams *) malloc(sizeof(*streamnames));
		if (streamnames == NULL)
		{
			perror("streamnames malloc failed");
			exit(-1);
		}
		streamtail = streamnames;
	}
	else
	{
		streamtail->next = (struct streams *) malloc(sizeof(*streamnames));
		if (streamtail->next == NULL) 
		{
			perror("streamtail->next malloc failed");
			exit(-1);
		}
		streamtail = streamtail->next;
	}
	streamtail->filename = filename;
	streamtail->offset = offset;
	streamtail->srcip = srcip;
	streamtail->srcport = srcport;
	streamtail->dstip = dstip;
	streamtail->dstport = dstport;
	streamtail->next = NULL;
}

/* takes a file handle to a pcap file, the size of the pcap file in bytes, */
/* and a struct from the streams struct chain, and prints the email data */

/* this is rather inefficient, since we are jumping from the "found" packet */
/* in the dumpit struct to find the start of the conversation in the pcap */
/* and then simply using the tcp tuple (src,dst, and ports) to find one side */
/* of the conversation - the sender side. I do look for a FIN/ACK and stop */
/* there, but if that isn't there then we end up moving through the entire */
/* file regardless of size. good enough, it works. */
void dump_data(FILE* pcap_file,unsigned int filesize,struct streams *dumpit,char * filename)
{
	BYTE buffer[MAXBUF];
	BYTE bufferdata[MAXBUF];
	pcap_hdr_t global_header;
	pcaprec_hdr_t record_header;
	struct stat statbuf;
	struct in_addr ip_addr_s;
	struct in_addr ip_addr_d;
	int i, j = 0, k, found_in_loop = 0, found_reply = 0, size, loop, bufstart, tcphdrlen, data_found = 0, print_headerline = 0;
	uint32 current_offset = 0;
	uint32 offset, srcip, dstip, seqnum;
	uint16 srcport, dstport;
	char *p, *o;


	/* this union is ugly and not perfect, but gets the job done */
	union
	{
		BYTE temp_four[4];
		uint32 temp_long;
	} conv_long;

	/* the offset in dumpit is the packet found, we need to back up to */
	/* get the pcap record_header so we know the size of our packet */
	current_offset = dumpit->offset - sizeof(record_header);

	/* move to the start of the conversation */
	size = fseek(pcap_file,current_offset,SEEK_SET);
	if(size < 0)
	{
		fprintf(stderr,"\n*** Error seeking to offset %x\n",current_offset);
		exit(-1);
	}

	/* loop which dumps ascii text of incoming data, breaks if EOF or we hit */
	/* a FIN/ACK in the conversation */
	while(1)
	{
		/* if EOF */
		if(filesize <= current_offset)
			break;
		/* read in the the packet's record_header from the pcap file */
		size = fread(&record_header,sizeof(record_header),1,pcap_file);
		if(size <= 0)
		{
			fprintf(stderr,"\n*** Error reading pcap record header\n");
			exit(-1);
		}

		/* update our counter */
		current_offset += sizeof(record_header);

		/* set up our buffers */
		guaranteed_memset(buffer,0,MAXBUF);
		guaranteed_memset(bufferdata,0,MAXBUF);
				
		/* we read in the packet */
		if(record_header.incl_len < 1600)
		{
			size = fread(buffer,1,record_header.incl_len,pcap_file);
			if(size <= 0)
			{
				fprintf(stderr,"\n*** Error reading record data\n");
				exit(-1);
			}
			/* update our counter */
			current_offset += record_header.incl_len;
		}

		/* if this isn't an IP packet, check the next one */
		if(!(buffer[12] == 0x08 && buffer[13] == 0x00)) 
			continue;
		/* we're looking for TCP packets, otherwise check the next one */
		if(buffer[23] != 0x06)
			continue;
		/* now get the tuple */
		/* convert source IP address */
		for(i=0;i<4;i++) conv_long.temp_four[i] = buffer[i+26];
		srcip = conv_long.temp_long;
		/* convert destination IP address */
		for(i=0;i<4;i++) conv_long.temp_four[i] = buffer[i+30];
		dstip = conv_long.temp_long;
		/* convert source port */
		srcport = (buffer[34] * 256) + buffer[35];
		/* convert destination port */
		dstport = (buffer[36] * 256) + buffer[37];
		/* match our packet with the stream we're looking at */
		if(((srcip == dumpit->srcip) && (srcport == dumpit->srcport) &&
		    (dstip == dumpit->dstip) && (dstport == dumpit->dstport)) ||
		   ((srcip == dumpit->dstip) && (srcport == dumpit->dstport) &&
		    (dstip == dumpit->srcip) && (dstport == dumpit->srcport)))
		{
			/* if we defined a target port we need to check for that */
			if (targ_port[0] > 0)
			{
				/* if no match, check next packet */
				if (!((buffer[36] == 0x00 && buffer[37] == targ_port[0]) || 
				      (buffer[34] == 0x00 && buffer[35] == targ_port[0])))
					continue;
			}
			/* now see if there is any data */
			if(buffer[16] == 0x00 && buffer[17] == 0x34)
			{
				if(buffer[47] == 0x11) break; /* FIN ACK, done */
				continue; /* otherwise check another packet */
			}
			/* need to find start of data, headers can vary, we'll get */
			/* the length of the packet by adding the total length value */
			/* from the IP header with the size of the ethernet header */
			/* (14) to get the max value for the loop */
			loop = (buffer[16] * 256) + buffer[17] + 14;
			/* tcp header length will vary and it is in the first 6
			   bits so shift left to get just our 6 bits */
			tcphdrlen = buffer[46] >> 2;
			/* we subtract 14 ethernet header bytes, the 20 IP header */
			/* bytes, and the tcp header length from loop, which gives */
			/* us the size of the data part of the packet. subtract */
			/* that value from loop and you have bufstart set to the */
			/* offset of the start of data */
			bufstart = (loop - 34) - tcphdrlen;
			bufstart = loop - bufstart;

			if(!print_headerline)
			{
				ip_addr_s.s_addr = srcip;
				ip_addr_d.s_addr = dstip;
				fprintf(stdout,"------\nFILE: %s OFFSET: %d\n",filename,dumpit->offset);
				fprintf(stdout,"SRC: %s : %d - ",inet_ntoa(ip_addr_s),srcport);
				fprintf(stdout,"DST: %s : %d\n\n",inet_ntoa(ip_addr_d),dstport);
				print_headerline = 1;
			}
			/* print the data from the packet */
			for(i=bufstart;i<loop;i++)
			{
				/* if printable ascii, print it */
				if((buffer[i] > 0x08 && buffer[i] < 0x0e) || (buffer[i] > 0x19) && (buffer[i] < 0x7f))
					fprintf(stdout,"%c",buffer[i]);
			}
		}
	}
	if(verbose) fprintf(stdout,"\n------\n");
}

int dump_conversations(FILE* pcap_file,char * filename)
{
	int streams = 0;
	struct streams *conversations;
	struct stat statbuf;

	fstat(fileno(pcap_file),&statbuf);
	
	if(verbose) fprintf(stdout,"Data from %s\n------\n",filename);
	conversations = streamnames;
	while(conversations != NULL)
	{
		streams++;
		dump_data(pcap_file,(unsigned int)statbuf.st_size,conversations,filename);
		conversations = conversations->next;
	}
	return streams;
}

/* analyze_conversations - Called by main, returns number of conversations */
/* in a PCAP file. */
/* --------------------- */
/* Analyze all conversations, this takes a pcap file and locates the start */
/* of all of the conversations by finding SYN packets. This is temp stored */
/* in a chained struct which is not saved, just used for this run. If a    */
/* specific protocol (SMTP or HTTP) is being searched for, we only include */
/* those conversations in the chained struct. */
int analyze_conversations(FILE* pcap_file,char * filename)
{
	int i, streams = 0;
	BYTE buffer[MAXBUF];
	pcap_hdr_t global_header;
	pcaprec_hdr_t record_header;
	struct stat statbuf;
	int size, loop;
	uint32 icount = 0, discard = 0, iframecount = 0, syns = 0, current_offset = 0;
	uint32 offset, srcip, dstip, seqnum;
	uint16 srcport, dstport;
	/* ugly union */
	union
	{
		BYTE temp_four[4];
		uint32 temp_long;
	} conv_long;

	/* fstat to get the file size */
	fstat(fileno(pcap_file),&statbuf);
	
	/* the global header of the pcap file, we'll skip this. if we need a check */
        /* to ensure we're looking at a pcap file, it would be here. */
 	size = fread(&global_header,sizeof(global_header),1,pcap_file);
	if(size<=0)
	{
		fprintf(stderr,"*** Error reading file\n");
		exit(-1);
	}

	/* icount is our counter we use to check EOF */
	icount += sizeof(global_header);
	while(1)
	{
		/* if we've reached EOF, we're done */
		if(statbuf.st_size <= icount)
			break;
		/* read in the record header, one per packet in the pcap file */
		size = fread(&record_header,sizeof(record_header),1,pcap_file);
		if(size <= 0)
		{
			fprintf(stderr,"\n*** Error reading record header at offset %u\n",icount);
			exit(-1);
		}

		/* update the counter */
		icount += sizeof(record_header);

		/* we'll use this if the packet matches */
		current_offset = icount;

		/* generic counter for the number of packets (frames) we process */
		iframecount++;

		/* set up our buffer */
		guaranteed_memset(buffer,0,MAXBUF);

		/* read in a packet */
		if(record_header.incl_len < 1600)
		{
			size = fread(buffer,1,record_header.incl_len,pcap_file);
			if(size <= 0)
			{
				fprintf(stderr,"\n*** Error reading record data at offset %u\n",icount);
				exit(-1);
			}
		}

		/* update the counter */
		icount += size;

		/* we are only going to seriously examine TCP packets */
		if((buffer[12] == 0x08 && buffer[13] == 0x00) && /* packet is an IP packet */
			(buffer[23] == 0x06))                    /* packet is a TCP packet */
		{
			/* if the packet is to and from the same address or a broadcast */
                        /* packet, ignore. yes it can happen for whatever reason but we */
                        /* will pretend it doesn't exist */
			if(((buffer[26] == buffer[30]) &&
					(buffer[27] == buffer[31]) &&
					(buffer[28] == buffer[32]) &&
					(buffer[29] == buffer[33])) ||
					(buffer[26] == 0xff) || 
					(buffer[30]== 0xff))
			{
				/* track stuff we discard, handy for debugging */
				discard += sizeof(record_header);
				discard += size;
				continue;
			}
			/* since we are only looking at specific traffic, we are trying to find */
                        /* the start of conversations, we are going to focus on traffic with */
                        /* our targeted source port and if the SYN flag only is set */
			if(buffer[47] == 0x02) syns++; /* counts all conversations */
			/* if we have a target port match... */
			if(((targ_port[0]>0) && (buffer[36] == 0x00) && (buffer[37] == targ_port[0]) && buffer[47] == 0x02) ||
			(buffer[47] == 0x02)) /* ...or just a SYN packet... */
			/* ...analyze it */
			{
				/* current offset in pcap file */
				offset = current_offset;
				/* convert source IP address */
				for(i=0;i<4;i++) conv_long.temp_four[i] = buffer[i+26];
				srcip = conv_long.temp_long;
				/* convert destination IP address */
				for(i=0;i<4;i++) conv_long.temp_four[i] = buffer[i+30];
				dstip = conv_long.temp_long;
				/* convert source port */
				srcport = (buffer[34] * 256) + buffer[35];
				/* convert destination port */
				dstport = (buffer[36] * 256) + buffer[37];
				/* the above four should be unique per conversation, but */
       	                        /* just in case we'll grab the initial sequence number */
				for(i=0;i<4;i++) conv_long.temp_four[i] = buffer[i+38];
				seqnum = conv_long.temp_long;
				/* time to populate our streams struct chain */
				populate_streams(filename,offset,srcip,srcport,dstip,dstport);
				/* increment the number of streams we found */
				streams++;
			}
		}
		else
		{
			discard += sizeof(record_header);
			discard += size;
		}
	}
	if(verbose) fprintf(stdout,"Bytes read: %d Bytes discarded: %d Conversations: %d Frame count: %d\n",icount,discard,syns,iframecount);
	return streams;
}

/*
 * usage
 */
void usage(char *prog)
{
	fprintf(stderr,"USAGE: ");
	fprintf(stderr,"%s [opts] [-f filename]\n\n",prog);
	fprintf(stderr,"  opts are h i v w\n");
	fprintf(stderr,"   -e email (SMTP) only\n");
	fprintf(stderr,"   -h this help screen\n");
	fprintf(stderr,"   -v verbose\n");
	fprintf(stderr,"   -w web (HTTP) only\n");
	fprintf(stderr,"\n");
}

/* main */
int main(int argc, char **argv)
{
	char *prog;
	extern char *optarg;
	extern int optind;
	extern int optopt;
	extern int opterr;
	char ch;
	char *filename;
	char *needle;
	int f_file = 0, s_file = 0;
	FILE* pcap_file;
	int stream;

	/* set up some local defaults */
	prog = argv[0];
	verbose = 0;
	needle = NULL;
	/* set up some global defaults */
	prot = "TCP";
	e_flag = 0;
	w_flag = 0;
	targ_port[0] = 0;

        /* process command line arguments */
	while ((ch = getopt(argc, argv, "ef:hvw")) != EOF) 
	switch(ch)
	{
		case 'e':
			e_flag = 1;
			prot = "SMTP";
			targ_port[0] = 25;
			break;
		case 'f':
			filename = optarg;
			f_file = 1;
			break;
		case 'h':
			usage(prog);
			exit(0);
		case 'v':
			verbose = 1;
			break;
		case 'w':
			w_flag = 1;
			prot = "HTTP";
			targ_port[0] = 80;
			break;
		default:
			usage(prog);
			exit(-1);
	}
	argc -= optind;
	argv += optind;

        /* we have to have something to do */
	if(!f_file)
	{
		fprintf(stderr,"*** Must choose a filename (-f) to read from\n");
		usage(prog);
		exit(-1);
	}

	/* analyze a pcap file for traffic, and optionally dump out data */

	/* get file handle */
	pcap_file = get_file_handle(filename,MODE_READ);
	
	/* this counts the conversations found */
	stream = analyze_conversations(pcap_file,filename);
	if (verbose) fprintf(stdout,"%d %s conversations found in %s\n",stream,prot,filename);

	/* dump all to stdout */
	stream = dump_conversations(pcap_file,filename);
	if (verbose) fprintf(stdout,"%d %s conversations dumped from %s\n",stream,prot,filename);

	fclose(pcap_file);
	exit(0);
}
